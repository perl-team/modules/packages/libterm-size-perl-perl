libterm-size-perl-perl (0.031-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Fix FTCBFS: Set ARCHLIB for the host architecture.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Update lintian override comment.

 -- gregor herrmann <gregoa@debian.org>  Tue, 02 Nov 2021 19:03:33 +0100

libterm-size-perl-perl (0.031-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Add debian/upstream/metadata.
  * Drop two patches which have been applied or fixed otherwise upstream.
  * Refresh remaining patch (offset).
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.
  * Drop build dependency on libtest-pod-perl.
    The respective test is an author test now.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Mar 2018 18:34:02 +0100

libterm-size-perl-perl (0.029-3) unstable; urgency=medium

  * Team upload.
  * Install Term::Size::Perl::Params into $Config{vendorarch}.
    It is created during build and contains arch-specific constants.
    (Closes: #809398)

 -- gregor herrmann <gregoa@debian.org>  Thu, 31 Dec 2015 01:41:51 +0100

libterm-size-perl-perl (0.029-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add patch to make builds reproducible. Remove the comment about the
    creation time from the template. Thanks to Chris Lamb for the bug
    report. (Closes: #778270)
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Fri, 01 May 2015 16:43:38 +0200

libterm-size-perl-perl (0.029-1) unstable; urgency=low

  * Initial Release. (Closes: #656798)

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sat, 21 Jan 2012 21:18:17 +0100
